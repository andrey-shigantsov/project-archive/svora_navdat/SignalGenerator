#include "QSdrUdpOutput.h"
#include <QDataStream>

#include <sdr_math.h>

QSdrUdpOutput::QSdrUdpOutput(Frequency_t Fd, int SamplesCountForBuf, QObject *parent) :
  QSdrOutput(parent)
{
  udp = 0;
  Timeout_us = SamplesCountForBuf*1000000/Fd;
}

bool QSdrUdpOutput::setIpConfig(QHostAddress Addr, quint16 Port)
{
  udpAddr = Addr;
  udpPort = Port;
  return true;
}

void QSdrUdpOutput::create()
{
  remove();

  udp = new QUdpSocket();
  udpSeqNum = 0;

  Timer = new QTimer();
  Timer->moveToThread(thread());
  connect(Timer, SIGNAL(timeout()), this, SLOT(processor()));
  Timer->start(Timeout_us/1000);
  return;
}

void QSdrUdpOutput::remove()
{
  if (udp)
  {
    disconnect(Timer, SIGNAL(timeout()), this, SLOT(processor()));
    Timer->stop();
    Timer->deleteLater();

    udp->deleteLater();
    udp = 0;
  }
}

void QSdrUdpOutput::processor()
{
  emit bufferReady();
  if(NeedForStop)
    emit readyForStop();
}

void QSdrUdpOutput::write_samples(iqSample_t* samples, Size_t count)
{
  QByteArray buf;
  QDataStream in(&buf, QIODevice::WriteOnly);
  in.setVersion(QDataStream::Qt_5_5);
  in.setFloatingPointPrecision(QDataStream::SinglePrecision);
  in << udpSeqNum++;
  for (Size_t i = 0; i < count; i++)
  {
    in << (float)samples[i].i*Gain;
    in << (float)samples[i].q*Gain;
  }
  qint64 size = udp->writeDatagram(buf.data(), buf.size(), udpAddr, udpPort);
  if (size != buf.size())
    qWarning() << QString("OUTPUT: UDP: write failure: size = %1").arg(size);
}
