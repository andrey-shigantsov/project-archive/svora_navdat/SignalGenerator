#ifndef LEDINDICATOR_H
#define LEDINDICATOR_H

#include <QWidget>

namespace Ui {
class LedIndicator;
}

class LedIndicator : public QWidget
{
  Q_OBJECT

public:
  explicit LedIndicator(QWidget *parent = 0);
  ~LedIndicator();

public slots:
  void setOutputRunningState();
  void setOutputStoppedState();
  void setNavdatTransmittingState();
  void setNavdatStoppedState();
  void setNavtexTransmittingState();
  void setNavtexStoppedState();

private:
  Ui::LedIndicator *ui;

  QString genStyleSheetWithColor(QColor color);
};

#endif // LEDINDICATOR_H
