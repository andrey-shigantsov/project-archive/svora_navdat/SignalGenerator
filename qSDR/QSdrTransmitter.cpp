#include "QSdrTransmitter.h"

QSdrTransmitter::QSdrTransmitter(Frequency_t Fd, QObject *parent) : QObject(parent)
{
  this->Fd = Fd;

  flag_isEnabled = 0;
  flag_isTransmit = 0;
}
