﻿#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QSettings>
#include <QFileDialog>
#include <QFile>
#include <QFileInfo>

#include "sdr_fft.h"
#include <SDR/Qt_Addons/common.h>

#define Fd (NAVDAT_FFT_SIZE_FACTOR*12000)

#define SHOW_MESSAGE_TIMEOUT 30000

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  settings("NAVDAT", "SignalsGenerator"),
  sdrOutputDriver(Fd, NAVDAT_SYMBOLSIZE),
  sdrAudioOutput(Fd),
  sdrUdpOutput(Fd, NAVDAT_SYMBOLSIZE),
  testTransmitter(Fd, NAVDAT_SYMBOLSIZE),
  navdatTransmitter(Fd),
  navtexTransmitter(Fd)
{
  init_sdrQtAddons();

  ui->setupUi(this);
  refresh_audio_devices();
  set_ui_started(false);
  ui->action_testStop->setEnabled(false);

  /* Индикаторы */

  ui->statusBar->addPermanentWidget(&leds);
  connect(&sdrOutputDriver, SIGNAL(started()), &leds, SLOT(setOutputRunningState()));
  connect(&sdrOutputDriver, SIGNAL(stopped()), &leds, SLOT(setOutputStoppedState()));
  connect(&navdatTransmitter, SIGNAL(transmit_running()), &leds, SLOT(setNavdatTransmittingState()));
  connect(&navdatTransmitter, SIGNAL(transmit_finished()), &leds, SLOT(setNavdatStoppedState()));
  connect(&navtexTransmitter, SIGNAL(transmit_running()), &leds, SLOT(setNavtexTransmittingState()));
  connect(&navtexTransmitter, SIGNAL(transmit_finished()), &leds, SLOT(setNavtexStoppedState()));
  connect(&navdatTransmitter, SIGNAL(frame_transmitted(int,int,bool,bool)), this, SLOT(navdat_frame_transmitted(int,int,bool,bool)));

  /* NAVDAT */

  ui->widget_NavdatState->setVisible(false);
  ui->navdatFilterID->setMinimum(0);
  ui->navdatFilterID->setMaximum(999);
  ui->navdatFilterID->setMinimum(0);
  on_navdatFilter_activated(0);
  connect(ui->button_navdatSendFile, SIGNAL(clicked(bool)), ui->action_NAVDAT_SendFile, SLOT(trigger()));

  navdatWorkingDir = settings.value("NAVDAT/WorkingDir", "").toString();
  if (QDir(navdatWorkingDir).exists())
    setNavdatWorkingDir(navdatWorkingDir);
  else
    on_action_NAVDAT_setFilePath_triggered();

  navdatTestEnable = false;
  navdatTestFileIsExt = false;

  navdat_MMSI_group_ID = 0;
  navdat_MMSI_group_MID = 273;
  navdat_MMSI_ID = 0;
  navdat_MMSI_MID = 273;

  connect(&navdatTransmitter, SIGNAL(enabled()), this, SLOT(navdat_state_widget_init()));
  connect(&navdatTransmitter, SIGNAL(disabled()), this, SLOT(navdat_state_widget_deinit()));
  connect(&navdatTransmitter, SIGNAL(transmit_finished()), this, SLOT(navdat_data_transmitted()));

  on_action_tisModeQam4_triggered();
  on_action_dsModeQam16_triggered();
  on_action_dsCodeMax_triggered();

  /* NAVTEX */

  connect(&navtexTransmitter, SIGNAL(transmit_finished()), this, SLOT(navtex_data_transmitted()));

  /* ТЕСТ */

  testTransmitter.setMode(QSdrTestTransmitter::mode_Constant);

  /* Каналы */

  chIdx_NAVDAT = createChannel(&navdatTransmitter, "NAVDAT", 0, 10000, 1);
  chIdx_NAVTEX = createChannel(&navtexTransmitter, "NAVTEX", 18000, 170, 1);
  chIdx_Test = createChannel(&testTransmitter, "Test", 440, 0, 1);

  /* Вывод */

  sdrAudioOutput.setGain_dB(ui->generalGain->value());
  sdrUdpOutput.setGain_dB(ui->generalGain->value());
  sdrOutputDriver.Multiplexer()->setAwgnNoiseLevel_dB(ui->noiseAwgnSNR->value());
  connect(ui->generalGain, SIGNAL(valueChanged(double)), &sdrAudioOutput, SLOT(setGain_dB(double)));
  connect(ui->generalGain, SIGNAL(valueChanged(double)), &sdrUdpOutput, SLOT(setGain_dB(double)));
  connect(ui->noiseAwgnSNR, SIGNAL(valueChanged(double)), sdrOutputDriver.Multiplexer(), SLOT(setAwgnNoiseLevel_dB(double)));
  connect(&sdrOutputDriver, SIGNAL(started()), this, SLOT(set_ui_started()));
  connect(&sdrOutputDriver, SIGNAL(stopped()), this, SLOT(set_ui_stopped()));

  /* Графики */

  OutputPlotsEnabled = false;
  outputSpectrumPlot = new SDR::TSpectrumPlot(Fd, 32768, ui->outputSpectrumPLot, this);
  outputSpectrumPlot->setBaseColors(QColor(20,20,20), Qt::white);
  outputSpectrumPlot->setAutoRefresh(false);
  outputSpectrumPlot->setAutoRangeY(true);
  outputSpectrumPlot->enableZoom(Qt::Horizontal);
  outputSpectrumPlot->enableDrag(Qt::Horizontal);
  connect(outputSpectrumPlot, SIGNAL(ready_for_refresh(QCPGraph*,QCPDataMap*,bool)), outputSpectrumPlot, SLOT(refresh_data(QCPGraph*,QCPDataMap*,bool)));
  connect(sdrOutputDriver.Multiplexer(), SIGNAL(samplesReady(iqSample_t*,Size_t)), this, SLOT(output_samples_handler(iqSample_t*,Size_t)), Qt::DirectConnection);

  /* ----- */

  setMode(RF_STAND);
}

MainWindow::~MainWindow()
{
  navdatTestEnable = false;
  sdrOutputDriver.stop();

  delete ui;
  delete outputSpectrumPlot;
}

void MainWindow::setMode(MainWindow::Mode mode)
{
  switch(mode)
  {
  case RF_STAND:
    ui->generalGain->setValue(16.0);
    ChannelsCfg[chIdx_NAVDAT]->setGain_dB(10.0);
    ChannelsCfg[chIdx_Test]->setGain_dB(-40.0);

    ui->outputSource->setCurrentIndex(0);
    ChannelsCfg[chIdx_NAVDAT]->setFreq(13000);
    ChannelsCfg[chIdx_Test]->setFreq(13010);

    ui->action_modeRF->setChecked(true);
    ui->action_modeLF->setChecked(false);
    ui->action_modeTest->setChecked(false);
    ui->action_modeLocalTest->setChecked(false);
    break;

  case LF_STAND:
    ui->generalGain->setValue(0.0);
    ui->outputSource->setCurrentIndex(0);
    ChannelsCfg[chIdx_NAVDAT]->setFreq(7000);
    ChannelsCfg[chIdx_Test]->setFreq(7010);

    ui->action_modeRF->setChecked(false);
    ui->action_modeLF->setChecked(true);
    ui->action_modeTest->setChecked(false);
    ui->action_modeLocalTest->setChecked(false);
    break;

  case TEST_LOCAL:
  case TEST:
    ui->generalGain->setValue(0.0);
    ui->outputSource->setCurrentIndex(1);
    ChannelsCfg[chIdx_NAVDAT]->setFreq(7000);
    ChannelsCfg[chIdx_Test]->setFreq(7010);

    ui->action_modeRF->setChecked(false);
    ui->action_modeLF->setChecked(false);

    if (mode == TEST)
    {
      ui->udpAddr->setCurrentIndex(0);
      ui->action_modeTest->setChecked(true);
      ui->action_modeLocalTest->setChecked(false);
    }
    else if (mode == TEST_LOCAL)
    {
      ui->udpAddr->setCurrentIndex(1);
      ui->action_modeTest->setChecked(false);
      ui->action_modeLocalTest->setChecked(true);
    }
    break;
  }
}

int MainWindow::createChannel(QSdrTransmitter* transmitter, QString name, Frequency_t freq, Frequency_t bandwidth, Sample_t gain)
{
  int idx = sdrOutputDriver.Multiplexer()->addChannel(freq, transmitter, gain);
  ChannelsCfg[idx] = new ChannelCfgWidget(idx, name, Fd, bandwidth, freq, gain, this);
  ui->layoutChannelsCfg->addWidget(ChannelsCfg[idx]);
  connect(ChannelsCfg[idx], SIGNAL(freq_changed(int,Frequency_t)), sdrOutputDriver.Multiplexer(), SLOT(setChannelFreq(int,Frequency_t)));
  connect(ChannelsCfg[idx], SIGNAL(gain_changed(int,Sample_t)), sdrOutputDriver.Multiplexer(), SLOT(setChannelGain(int,Sample_t)));
  return idx;
}

void MainWindow::setNavdatWorkingDir(QString path)
{
  navdatWorkingDir = path;
  refresh_navdatTestFileName();
}

QString MainWindow::navdatFileFullName(QString fileName)
{
  if (navdatWorkingDir.isEmpty())
    navdatWorkingDir = ".";
  return navdatWorkingDir + "/" + fileName;
}

QString MainWindow::navdatTestFileName()
{
  QString tisMode;
  if (ui->action_tisModeQam4->isChecked())
    tisMode = "4";
  else if (ui->action_tisModeQam16->isChecked())
    tisMode = "16";

  QString dsMode;
  if (ui->action_dsModeQam4->isChecked())
    dsMode = "4";
  else if (ui->action_dsModeQam16->isChecked())
    dsMode = "16";
  else if (ui->action_dsModeQam64->isChecked())
    dsMode = "64";

  QString code;
  if (ui->action_dsCodeMin->isChecked())
    code = "Min";
  else if (ui->action_dsCodeNorm->isChecked())
    code = "Norm";
  else if (ui->action_dsCodeMax->isChecked())
    code = "Max";

  return "test" + code + "_" + dsMode + "-" + tisMode;
}

void MainWindow::refresh_navdatTestFileName()
{
  if (!navdatTestFileIsExt)
  {
    QString fileName = navdatTestFileName();
    navdatTestFileFullName = navdatFileFullName(fileName);
    if (!QFile::exists(navdatTestFileFullName))
      navdatTestFileFullName = "";
  }
}

quint32 MainWindow::navdatMMSI()
{
  quint32 midFactor;
  switch (ui->navdatFilter->currentIndex())
  {
  default:
  case 0:
    return 0;

  case 1:
    midFactor = 100000;
    break;

  case 2:
    midFactor = 1000000;
    break;
  }
  return (ui->navdatFilterMID->value() % 1000)*midFactor + ui->navdatFilterID->value();
}

NAVDAT_MultiplexCfg_t MainWindow::getNavdatCfgMux()
{
  NAVDAT_MultiplexCfg_t cfg;

  cfg.codeXIS = NAVDAT_xIS_Code_Max;
  if (ui->action_dsCodeMin->isChecked())
    cfg.codeDS = NAVDAT_DS_Code_Min;
  else if (ui->action_dsCodeNorm->isChecked())
    cfg.codeDS = NAVDAT_DS_Code_Normal;
  else if (ui->action_dsCodeMax->isChecked())
    cfg.codeDS = NAVDAT_DS_Code_Max;

  if (ui->action_tisModeQam4->isChecked())
    cfg.modeTIS = NAVDAT_TIS_qam4;
  else if (ui->action_tisModeQam16->isChecked())
    cfg.modeTIS = NAVDAT_TIS_qam16;

  if (ui->action_dsModeQam4->isChecked())
    cfg.modeDS = NAVDAT_DS_qam4;
  else if (ui->action_dsModeQam16->isChecked())
    cfg.modeDS = NAVDAT_DS_qam16;
  else if (ui->action_dsModeQam64->isChecked())
    cfg.modeDS = NAVDAT_DS_qam64;

  return cfg;
}

void MainWindow::refresh_audio_devices()
{
  audioDevices = QAudioDeviceInfo::availableDevices(QAudio::AudioOutput);

  ui->audioDevice->clear();
  for (int i = 0; i < audioDevices.count(); i++)
    ui->audioDevice->addItem(audioDevices[i].deviceName());

  ui->audioDevice->setCurrentIndex(audioDevices.indexOf(QAudioDeviceInfo::defaultOutputDevice()));
}

#define BUTTON_OUTPUT_CONTROL_LABEL(isStarted) ( (isStarted) ? "Стоп" : "Запуск" )
#define OUTPUT_STATUS_MESSAGE(isStarted) ( (isStarted) ? "Вывод запущен" : "Вывод остановлен" )

void MainWindow::set_ui_started(bool flag)
{
  ui->outputSource->setEnabled(!flag);
  ui->widget_outputSourceCfg->setEnabled(!flag);

  ui->action_outputStart->setEnabled(!flag);
  ui->action_outputStop->setEnabled(flag);

  ui->button_outputControl->setText(BUTTON_OUTPUT_CONTROL_LABEL(flag));

  ui->statusBar->showMessage(OUTPUT_STATUS_MESSAGE(flag), SHOW_MESSAGE_TIMEOUT);
}

void MainWindow::navdat_state_widget_reset()
{
  ui->label_NavdatSate->clear();
  ui->NavdatProgressBar->setValue(0);
  ui->NavdatProgressBar->setMinimum(0);
  ui->NavdatProgressBar->setMaximum(0);
}

void MainWindow::navdat_state_widget_init()
{
  navdat_state_widget_reset();
  ui->widget_NavdatState->setVisible(true);
}

void MainWindow::navdat_state_widget_deinit()
{
  navdat_state_widget_reset();
  ui->widget_NavdatState->setVisible(false);
}

void MainWindow::start_output()
{
  switch(ui->outputSource->currentIndex())
  {
  case 0:{ // Аудио
    QAudioDeviceInfo info = audioDevices[ui->audioDevice->currentIndex()];
    if (!sdrAudioOutput.setDevice(info))
    {
      ui->statusBar->showMessage("Ошибка: не удалось настроить Аудио устройство", SHOW_MESSAGE_TIMEOUT);
      qInfo("OUTPUT: setAudioMode FAILURE");
      return;
    }
    sdrOutputDriver.setOutput(&sdrAudioOutput);
    break;}

  case 1: // UDP
    if (!sdrUdpOutput.setIpConfig(QHostAddress(ui->udpAddr->currentText()), ui->udpPort->value()))
    {
      ui->statusBar->showMessage("Ошибка: не удалось настроить UDP сокет", SHOW_MESSAGE_TIMEOUT);
      qInfo("OUTPUT: setUdpMode FAILURE");
      return;
    }
    sdrOutputDriver.setOutput(&sdrUdpOutput);
    break;
  }
  sdrOutputDriver.start();
}

void MainWindow::stop_output()
{
  sdrOutputDriver.stop();
}

void MainWindow::start_transmitter(QSdrTransmitter* transmitter)
{
  if (!sdrOutputDriver.isRunning())
    start_output();
  if (!transmitter->isTransmit())
  {
    transmitter->reset();
    transmitter->enable();
  }
}

void MainWindow::stop_transmitter(QSdrTransmitter* transmitter)
{
  transmitter->disable();
  sdrOutputDriver.stopIfNeeded();
}

void MainWindow::set_ui_started()
{
  set_ui_started(true);
}

void MainWindow::set_ui_stopped()
{
  set_ui_started(false);
}

void MainWindow::navdat_test_file_data(NAVDAT_MultiplexCfg_t cfgMux, QString& fileName, QByteArray& data, bool autosaveNewData)
{
  if (navdatTestFileFullName.isEmpty())
  {
    fileName = navdatTestFileName();
    data.resize(navdatTransmitter.maxDataInPacket(cfgMux));
    for (int i = 0; i < data.size(); ++i)
      data[i] = rand() % 256;

    navdatTestFileFullName = navdatFileFullName(fileName);
    QFile file(navdatTestFileFullName);
    if (!file.open(QIODevice::WriteOnly))
    {
      ui->statusBar->showMessage(QString("Не удалось записать тестовый файл: %1").arg(file.errorString()));
      return;
    }
    file.write(data);
    file.close();
    if (autosaveNewData)
      navdatTransmitter.saveToFile(cfgMux, navdatWorkingDir, fileName, data, 0, 0, 0xffff);
  }
  else
  {
    QFile file(navdatTestFileFullName);
    if (!file.open(QIODevice::ReadOnly))
    {
      on_action_navdatTestStop_triggered();
      ui->statusBar->showMessage(QString("Не удалось открыть тестовый файл: %1").arg(file.errorString()));
      return;
    }
    fileName = QFileInfo(navdatTestFileFullName).fileName();
    data = file.readAll();
    file.close();
  }
}

void MainWindow::navdat_test_transmit()
{
  NAVDAT_MultiplexCfg_t cfgMux = getNavdatCfgMux();
  QString fileName;
  QByteArray data;
  navdat_test_file_data(cfgMux, fileName, data);
  navdatTransmitter.transmit(cfgMux, fileName, data, 0, 0, 0xffff);
}

void MainWindow::navdat_frame_transmitted(int fileId, int id, bool isFirst, bool isLast)
{
  if (isFirst)
  {
    QSdrNavdatTransmitter::FileMetaData data = navdatTransmitter.getCurrentFileMetaData();
    ui->label_NavdatSate->setText(QString("передача файла \"%2\" (id#%1)").arg(fileId).arg(data.Name));
    ui->NavdatProgressBar->setMinimum(0);
    ui->NavdatProgressBar->setMaximum(data.FramesCount-1);
  }
  ui->NavdatProgressBar->setValue(id);
  if (isLast)
    ui->NavdatProgressBar->setMaximum(0);
}

void MainWindow::navdat_data_transmitted()
{
  if (navdatTestEnable)
    navdat_test_transmit();
  else
  {
    navdatTransmitter.disable();
    sdrOutputDriver.stopIfNeeded();
  }
}

void MainWindow::navtex_data_transmitted()
{
  navtexTransmitter.disable();
  sdrOutputDriver.stopIfNeeded();
}

void MainWindow::output_samples_handler(iqSample_t* samples, Size_t count)
{
  if (OutputPlotsEnabled)
    outputSpectrumPlot->append(samples, count);
}

void MainWindow::on_outputSource_currentIndexChanged(int index)
{
  ui->widget_outputSourceCfg->setCurrentIndex(index);
}

void MainWindow::on_button_outputControl_clicked()
{
  if (!sdrOutputDriver.isRunning())
    start_output();
  else
    stop_output();
}

void MainWindow::on_action_NAVDAT_SendFile_triggered()
{
  QString fullFileName = QFileDialog::getOpenFileName(this, "Открыть файл NAVDAT");
  QFile file(fullFileName);
  if (!file.open(QIODevice::ReadOnly))
    return;

  quint32 mmsi = navdatMMSI();

  qint16 FileId;
  if (ui->action_NAVDAT_NullFileId->isChecked())
    FileId = 0;
  else
    FileId = -1;

  quint16 timestamp;
  if (ui->action_NAVDAT_fixedTimestamp->isChecked())
    timestamp = 0xffff;
  else
    timestamp = 0;

  QString fileName = QFileInfo(fullFileName).fileName();
  QByteArray data = file.readAll();
  if(ui->action_NAVDAT_saveToFile->isChecked())
  {
    navdatTransmitter.saveToFile(getNavdatCfgMux(), navdatWorkingDir, fileName, data, mmsi, FileId, timestamp);
    return;
  }

  start_transmitter(&navdatTransmitter);
  navdatTransmitter.transmit(getNavdatCfgMux(), fileName, data, mmsi, FileId, timestamp);
}

void MainWindow::on_action_NAVDAT_reset_triggered()
{
  if (navdatTestEnable)
    on_action_navdatTestStop_triggered();
  else
  {
    navdatTransmitter.disable();
    navdatTransmitter.reset();
    sdrOutputDriver.stopIfNeeded();
  }
}

void MainWindow::on_action_NAVDAT_setTestFile_triggered()
{
  navdatTestFileFullName = QFileDialog::getOpenFileName(this, "Открыть тестовый файл");
  if (QFile::exists(navdatTestFileFullName))
    navdatTestFileIsExt = true;
}

void MainWindow::on_action_navdatTestStart_triggered()
{
  start_transmitter(&navdatTransmitter);
  navdatTestEnable = true;

  navdat_test_transmit();

  ui->action_navdatTestStart->setEnabled(false);
  ui->action_navdatTestStop->setEnabled(true);
}

void MainWindow::on_action_navdatTestStop_triggered()
{
  navdatTestEnable = false;

  ui->action_navdatTestStart->setEnabled(true);
  ui->action_navdatTestStop->setEnabled(false);
}

void MainWindow::on_action_testStart_triggered()
{
  start_transmitter(&testTransmitter);

  ui->action_testStart->setEnabled(false);
  ui->action_testStop->setEnabled(true);
}

void MainWindow::on_action_testStop_triggered()
{
  testTransmitter.disable();
  sdrOutputDriver.stopIfNeeded();

  ui->action_testStart->setEnabled(true);
  ui->action_testStop->setEnabled(false);
}

void MainWindow::on_action_testModeGenerator_triggered()
{
  testTransmitter.setMode(QSdrTestTransmitter::mode_Generator);

  ui->action_testModeGenerator->setChecked(true);
  ui->action_testModeConstant->setChecked(false);
}

void MainWindow::on_action_testModeConstant_triggered()
{
  testTransmitter.setMode(QSdrTestTransmitter::mode_Constant);

  ui->action_testModeGenerator->setChecked(false);
  ui->action_testModeConstant->setChecked(true);
}

void MainWindow::on_action_tisModeQam4_triggered()
{
  ui->action_tisModeQam4->setChecked(true);
  ui->action_tisModeQam16->setChecked(false);

  ui->navdatModeTIS->setCurrentIndex(0);

  refresh_navdatTestFileName();
}

void MainWindow::on_action_tisModeQam16_triggered()
{
  ui->action_tisModeQam4->setChecked(false);
  ui->action_tisModeQam16->setChecked(true);

  ui->navdatModeTIS->setCurrentIndex(1);

  refresh_navdatTestFileName();
}

void MainWindow::on_action_dsModeQam4_triggered()
{
  ui->action_dsModeQam4->setChecked(true);
  ui->action_dsModeQam16->setChecked(false);
  ui->action_dsModeQam64->setChecked(false);

  ui->navdatModeDS->setCurrentIndex(0);

  refresh_navdatTestFileName();
}

void MainWindow::on_action_dsModeQam16_triggered()
{
  ui->action_dsModeQam4->setChecked(false);
  ui->action_dsModeQam16->setChecked(true);
  ui->action_dsModeQam64->setChecked(false);

  ui->navdatModeDS->setCurrentIndex(1);

  refresh_navdatTestFileName();
}

void MainWindow::on_action_dsModeQam64_triggered()
{
  ui->action_dsModeQam4->setChecked(false);
  ui->action_dsModeQam16->setChecked(false);
  ui->action_dsModeQam64->setChecked(true);

  ui->navdatModeDS->setCurrentIndex(2);

  refresh_navdatTestFileName();
}

void MainWindow::on_action_dsCodeMin_triggered()
{
  ui->action_dsCodeMin->setChecked(true);
  ui->action_dsCodeNorm->setChecked(false);
  ui->action_dsCodeMax->setChecked(false);

  ui->navdatCodeDS->setCurrentIndex(0);

  refresh_navdatTestFileName();
}

void MainWindow::on_action_dsCodeNorm_triggered()
{
  ui->action_dsCodeMin->setChecked(false);
  ui->action_dsCodeNorm->setChecked(true);
  ui->action_dsCodeMax->setChecked(false);

  ui->navdatCodeDS->setCurrentIndex(1);

  refresh_navdatTestFileName();
}

void MainWindow::on_action_dsCodeMax_triggered()
{
  ui->action_dsCodeMin->setChecked(false);
  ui->action_dsCodeNorm->setChecked(false);
  ui->action_dsCodeMax->setChecked(true);

  ui->navdatCodeDS->setCurrentIndex(2);

  refresh_navdatTestFileName();
}

void MainWindow::on_action_NAVDAT_setFilePath_triggered()
{
  setNavdatWorkingDir(QFileDialog::getExistingDirectory(this, "Открыть рабочую папку NAVDAT", navdatWorkingDir));
  settings.setValue("NAVDAT/WorkingDir", navdatWorkingDir);
}

void MainWindow::on_action_NAVDAT_getTestFileData_triggered()
{
  NAVDAT_MultiplexCfg_t cfgMux = getNavdatCfgMux();
  QString fileName;
  QByteArray data;
  navdat_test_file_data(cfgMux, fileName, data, false);
  navdatTransmitter.saveToFile(cfgMux, navdatWorkingDir, fileName, data, 0, 0, 0xffff);
}

void MainWindow::on_action_NAVDAT_resetTestFileName_triggered()
{
  navdatTestFileIsExt = false;
  navdatTestFileFullName.clear();
  refresh_navdatTestFileName();
}

void MainWindow::on_action_modeRF_triggered()
{
  setMode(RF_STAND);
}

void MainWindow::on_action_modeLF_triggered()
{
  setMode(LF_STAND);
}

void MainWindow::on_action_modeTest_triggered()
{
  setMode(TEST);
}

void MainWindow::on_navdatCodeDS_activated(int index)
{
  switch(index)
  {
  case 0:
    on_action_dsCodeMin_triggered();
    break;

  case 1:
    on_action_dsCodeNorm_triggered();
    break;

  case 2:
    on_action_dsCodeMax_triggered();
    break;
  }
}

void MainWindow::on_navdatModeTIS_activated(int index)
{
  switch(index)
  {
  case 0:
    on_action_tisModeQam4_triggered();
    break;

  case 1:
    on_action_tisModeQam16_triggered();
    break;
  }
}

void MainWindow::on_navdatModeDS_activated(int index)
{
  switch(index)
  {
  case 0:
    on_action_dsModeQam4_triggered();
    break;

  case 1:
    on_action_dsModeQam16_triggered();
    break;

  case 2:
    on_action_dsModeQam64_triggered();
    break;
  }
}

void MainWindow::on_navdatFilter_activated(int index)
{
  switch (index)
  {
  case 0:
    ui->widget_navdatFilter->setVisible(false);
    break;

  case 1:
    ui->widget_navdatFilter->setVisible(true);
    ui->navdatFilterID->setMaximum(99999);
    ui->navdatFilterID->setValue(navdat_MMSI_group_ID);
    ui->navdatFilterMID->setValue(navdat_MMSI_group_MID);
    break;

  case 2:
    ui->widget_navdatFilter->setVisible(true);
    ui->navdatFilterID->setMaximum(999999);
    ui->navdatFilterID->setValue(navdat_MMSI_ID);
    ui->navdatFilterMID->setValue(navdat_MMSI_MID);
    break;
  }
}

void MainWindow::on_button_navtexSend_clicked()
{
  QByteArray header = ui->navtexHeader->text().toUpper().toLocal8Bit();

  start_transmitter(&navtexTransmitter);
  navtexTransmitter.send_message(header.at(0), header.at(1), ui->navtexIdx->value(), ui->navtexData->toPlainText());
}

void MainWindow::on_action_outputStart_triggered()
{
  start_output();
}

void MainWindow::on_action_outputStop_triggered()
{
  stop_output();
}

void MainWindow::on_action_modeLocalTest_triggered()
{
    setMode(TEST_LOCAL);
}

void MainWindow::on_tabWidget_currentChanged(int index)
{
  switch(index)
  {
  case 0:
    if (OutputPlotsEnabled)
      OutputPlotsEnabled = false;
    break;
  case 1:
    if (OutputPlotsEnabled) break;
    OutputPlotsEnabled = true;
    outputSpectrumPlot->clear();
    break;
  }
}

void MainWindow::on_navdatFilterID_editingFinished()
{
  switch(ui->navdatFilter->currentIndex())
  {
  case 1:
    navdat_MMSI_group_ID = ui->navdatFilterID->value();
    break;

  case 2:
    navdat_MMSI_ID = ui->navdatFilterID->value();
    break;
  }
}

void MainWindow::on_navdatFilterMID_editingFinished()
{
  switch(ui->navdatFilter->currentIndex())
  {
  case 1:
    navdat_MMSI_group_MID = ui->navdatFilterID->value();
    break;

  case 2:
    navdat_MMSI_MID = ui->navdatFilterID->value();
    break;
  }
}
