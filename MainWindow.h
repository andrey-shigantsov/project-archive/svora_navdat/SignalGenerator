#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMap>

#include "ChannelCfgWidget.h"

#include "LedIndicator.h"

#include <QSdrOutputDriver.h>
#include <QSdrAudioOutput.h>
#include <QSdrUdpOutput.h>

#include <QSdrTestTransmitter.h>
#include <QSdrNavdatTransmitter.h>
#include <QSdrNavtexTransmitter.h>

#include <SDR/Qt_Addons/TScopePlot.h>
#include <SDR/Qt_Addons/TSpectrumPlot.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

signals:

private:
  Ui::MainWindow *ui;
  QSettings settings;
  LedIndicator leds;

  QSdrOutputDriver sdrOutputDriver;
  QSdrAudioOutput sdrAudioOutput;
  QSdrUdpOutput sdrUdpOutput;

  QSdrTestTransmitter testTransmitter;
  QSdrNavdatTransmitter navdatTransmitter;
  QSdrNavtexTransmitter navtexTransmitter;

  int chIdx_Test, chIdx_NAVDAT, chIdx_NAVTEX;

  QString navdatWorkingDir, navdatTestFileFullName;
  bool navdatTestEnable, navdatTestFileIsExt;
  int navdat_MMSI_group_MID, navdat_MMSI_MID;
  int navdat_MMSI_group_ID, navdat_MMSI_ID;

  QMap<int,ChannelCfgWidget*> ChannelsCfg;

  QList<QAudioDeviceInfo> audioDevices;

  bool OutputPlotsEnabled;
  SDR::TSpectrumPlot* outputSpectrumPlot;

  enum Mode {RF_STAND, LF_STAND, TEST, TEST_LOCAL};
  void setMode(Mode mode);

  int createChannel(QSdrTransmitter* transmitter, QString name, Frequency_t freq, Frequency_t bandwidth, Sample_t gain);

  void setNavdatWorkingDir(QString path);
  QString navdatFileFullName(QString fileName);
  QString navdatTestFileName();
  void refresh_navdatTestFileName();
  quint32 navdatMMSI();
  NAVDAT_MultiplexCfg_t getNavdatCfgMux();

private slots:
  void refresh_audio_devices();
  void start_output();
  void stop_output();
  void start_transmitter(QSdrTransmitter* transmitter);
  void stop_transmitter(QSdrTransmitter* transmitter);
  void set_ui_started();
  void set_ui_stopped();
  void set_ui_started(bool flag);

  void navdat_state_widget_reset();
  void navdat_state_widget_init();
  void navdat_state_widget_deinit();
  void navdat_test_file_data(NAVDAT_MultiplexCfg_t cfgMux, QString& fileName, QByteArray& data, bool autosaveNewData = true);
  void navdat_test_transmit();
  void navdat_frame_transmitted(int fileId, int id, bool isFirst, bool isLast);
  void navdat_data_transmitted();
  void navtex_data_transmitted();
  void output_samples_handler(iqSample_t* samples, Size_t count);

  void on_action_NAVDAT_SendFile_triggered();
  void on_action_testStart_triggered();
  void on_action_testStop_triggered();
  void on_button_outputControl_clicked();
  void on_action_testModeGenerator_triggered();
  void on_action_testModeConstant_triggered();
  void on_action_navdatTestStart_triggered();
  void on_action_navdatTestStop_triggered();
  void on_outputSource_currentIndexChanged(int index);
  void on_action_NAVDAT_reset_triggered();
  void on_action_tisModeQam4_triggered();
  void on_action_tisModeQam16_triggered();
  void on_action_dsModeQam4_triggered();
  void on_action_dsModeQam16_triggered();
  void on_action_dsModeQam64_triggered();
  void on_action_dsCodeMin_triggered();
  void on_action_dsCodeNorm_triggered();
  void on_action_dsCodeMax_triggered();
  void on_action_NAVDAT_setTestFile_triggered();
  void on_action_NAVDAT_setFilePath_triggered();
  void on_action_NAVDAT_getTestFileData_triggered();
  void on_action_NAVDAT_resetTestFileName_triggered();
  void on_action_modeRF_triggered();
  void on_action_modeLF_triggered();
  void on_action_modeTest_triggered();
  void on_navdatCodeDS_activated(int index);
  void on_navdatModeTIS_activated(int index);
  void on_navdatModeDS_activated(int index);
  void on_navdatFilter_activated(int index);
  void on_button_navtexSend_clicked();
  void on_action_outputStart_triggered();
  void on_action_outputStop_triggered();
  void on_action_modeLocalTest_triggered();
  void on_tabWidget_currentChanged(int index);
  void on_navdatFilterID_editingFinished();
  void on_navdatFilterMID_editingFinished();
};

#endif // MAINWINDOW_H
