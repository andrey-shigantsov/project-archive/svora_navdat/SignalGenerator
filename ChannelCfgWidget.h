#ifndef CHANNELCFGWIDGET_H
#define CHANNELCFGWIDGET_H

#include <QWidget>
#include <sdr_basic.h>

namespace Ui {
class ChannelCfgWidget;
}

class ChannelCfgWidget : public QWidget
{
  Q_OBJECT

public:
  explicit ChannelCfgWidget(int idx, QString Name, Frequency_t fd, Frequency_t bandwidth, Frequency_t freq, Sample_t gain, QWidget *parent = 0);
  ~ChannelCfgWidget();

  void setName(QString name);
  void setFreqParams(Frequency_t fd, int bandwidth);
  void setFreq(Frequency_t freq);
  void setGain_dB(Sample_t gain);
  void setGain(Sample_t gain);

signals:
  void freq_changed(int idx, Frequency_t freq);
  void gain_changed(int idx, Sample_t gain);

private slots:
  void on_Freq_valueChanged(double arg1);
  void on_Gain_valueChanged(double arg1);

private:
  Ui::ChannelCfgWidget *ui;
  int Idx;
};

#endif // CHANNELCFGWIDGET_H
