#ifndef QSDRTRANSMITTER_H
#define QSDRTRANSMITTER_H

#include <QObject>
#include "SDR/BASE/common.h"

class QSdrTransmitter : public QObject
{
  Q_OBJECT

  friend class QSdrOutput;
public:
  explicit QSdrTransmitter(Frequency_t Fd, QObject *parent = 0);

  virtual void reset() = 0;

  bool isEnabled(){return flag_isEnabled;}
  virtual void enable(){flag_isEnabled = 1; emit enabled();}
  virtual void disable(){flag_isEnabled = 0; emit disabled();}

  bool isTransmit(){return flag_isTransmit;}

  virtual void stream_out(iqSample_t* samples, Size_t count) = 0;

signals:
  void enabled();
  void disabled();
  void transmit_running();
  void transmit_finished();

protected:
  void setIsTransmit(bool flag){flag_isTransmit = flag;if(flag) emit transmit_running(); else emit transmit_finished();}

private:
  bool flag_isEnabled, flag_isTransmit;
  Frequency_t Fd;
};

#endif // QSDRTRANSMITTER_H
