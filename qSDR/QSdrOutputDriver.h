#ifndef QSDROUTPUTDRIVER_H
#define QSDROUTPUTDRIVER_H

#include <QObject>
#include <QThread>

#include <QSdrChannelsMultiplexer.h>
#include <QSdrOutput.h>

class QSdrOutputDriver : public QObject
{
  Q_OBJECT
public:
  explicit QSdrOutputDriver(Frequency_t Fd, int SamplesCountForBuf, QObject *parent = 0);

  QSdrChannelsMultiplexer* Multiplexer(){return &Mux;}
  void setOutput(QSdrOutput* Output);
  bool isRunning(){return Thread.isRunning();}

signals:
  void started();
  void stopped();

public slots:
  void start();
  void stop();
  void stopIfNeeded();

private:
  QThread Thread;
  QSdrChannelsMultiplexer Mux;
  QSdrOutput* Output;

  bool flag_Worked;

  void enableOutput(QSdrOutput* Output);
  void disableOutput(QSdrOutput* Output);
};

#endif // QSDROUTPUTDRIVER_H
