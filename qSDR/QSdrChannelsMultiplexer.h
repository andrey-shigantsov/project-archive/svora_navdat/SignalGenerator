#ifndef QSDRCHANNELSMULTIPLEXOR_H
#define QSDRCHANNELSMULTIPLEXOR_H

#include <QSdrTransmitter.h>
#include <QList>
#include <QVector>

#include "SDR/BASE/Processors/UpConverter.h"
#include "SDR/BASE/Noises/awgn.h"

class QSdrChannelsMultiplexer : public QObject
{
  Q_OBJECT

public:
  explicit QSdrChannelsMultiplexer(Frequency_t Fd, int SamplesCountForBuf, QObject* parent = 0);
  ~QSdrChannelsMultiplexer();

  // Возвращает индекс канала
  int addChannel(Frequency_t freq, QSdrTransmitter* transmitter, Sample_t gain = 0.5);

public slots:
  bool isEnabledChannels();
  void stream_processor();
  void setChannelFreq(int index, Frequency_t freq);
  void setChannelGain(int index, Sample_t gain);
  void setAwgnNoiseLevel_dB(double level);

signals:
  void samplesReady(iqSample_t* samples, Size_t count);

private:
  Frequency_t Fd;
  Size_t SamplesCount;

  QList<QSdrTransmitter*> Transmitters;
  Noise_Awgn_t Awgn;

  QList<UpConverter_t*> UpConverters;
  QList<Sample_t> Gains;

  QVector<iqSample_t> procBuf, ucProcBuf, muxBuf;
};

#endif // QSDRCHANNELSMULTIPLEXOR_H
