#include "QSdrTestTransmitter.h"
#include "sdr_math.h"

QSdrTestTransmitter::QSdrTestTransmitter(Frequency_t Fd, int GeneratorBufSize, QObject* parent) :
  QSdrTransmitter(Fd, parent)
{
  GeneratorBuf.resize(GeneratorBufSize);

  GeneratorCfg_t cfgGenerator;
  cfgGenerator.Fd = Fd;
  cfgGenerator.F = 440;
  cfgGenerator.Buf.P = GeneratorBuf.data();
  cfgGenerator.Buf.Size = GeneratorBuf.size();
  init_Generator(&Generator, &cfgGenerator);

  mode = mode_Constant;
}

void QSdrTestTransmitter::reset()
{

}

void QSdrTestTransmitter::stream_out(iqSample_t* samples, Size_t count)
{
  switch (mode)
  {
  case mode_Generator:
    generator_iq(&Generator, samples, count);
    break;
  case mode_Constant:{
    iqSample_t x; x.i = 1; x.q = 0;
    constant_iq(&x, samples, count);
    break;}
  }
}
