#ifndef QSDRAUDIOOUTPUT_H
#define QSDRAUDIOOUTPUT_H

#define QSDR_AUDIO_OUTPUT_USE_TIMER

#include <QSdrOutput.h>
#include <QAudioOutput>
#ifdef QSDR_AUDIO_OUTPUT_USE_TIMER
#include <QTimer>
#endif

class QSdrAudioOutput : public QSdrOutput
{
  Q_OBJECT
public:
  explicit QSdrAudioOutput(Frequency_t Fd, QObject *parent = 0);

  bool setDevice(QAudioDeviceInfo DeviceInfo);

signals:

public slots:

private:
#ifdef QSDR_AUDIO_OUTPUT_USE_TIMER
  QTimer *Timer;
#endif
  QAudioOutput* audio;
  QAudioDeviceInfo audioDevice;
  QAudioFormat audioFormat;

  QByteArray audioCollector;
  QIODevice* audioIO;

  int sample_size;
  Sample_t max_value;
  void convert_sample(Sample_t sample, unsigned char*& ptr);

private slots:
  void create();
  void remove();
  void write_samples(iqSample_t* samples, Size_t count);
  void refresh_gain(Sample_t Gain){max_value = 1.0/Gain;}

  void processor();

  void audio_state_handler(QAudio::State state);
};

#endif // QSDRAUDIOOUTPUT_H
