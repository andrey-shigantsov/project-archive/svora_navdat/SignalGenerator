#ifndef QSDRNAVDATTRANSMITTER_H
#define QSDRNAVDATTRANSMITTER_H

#include "QSdrTransmitter.h"

#include "SDR/NAVDAT/Multiplex/Multiplex.h"
#include "SDR/NAVDAT/Modem/Modulator.h"
#include "SDR/NAVDAT/Codec/Codec.h"

#include <QVector>
#include <QList>

#define NAVDAT_FFT_A_FACTOR 1
#define NAVDAT_FFT_N NAVDAT_Nfft(NAVDAT_FFT_SIZE_FACTOR)

#define NAVDAT_SYMBOLSIZE NAVDAT_OFDM_SYMBOL_SAMPLES_COUNT(NAVDAT_FFT_SIZE_FACTOR)

class QSdrNavdatTransmitter : public QSdrTransmitter
{
  Q_OBJECT

public:
  struct FileMetaData
  {
    QString Name;
    int FramesCount;
  };

  explicit QSdrNavdatTransmitter(Frequency_t Fd, QObject *parent = 0);
  ~QSdrNavdatTransmitter();

  void fill_empty_buf_handler(iqSample_t* Samples, Size_t Count);
  void ifft_samples_handler(iqSample_t* Samples, Size_t Count);
  void data_sent_handler();

  void setSenderID(QString id){SenderID = id;}
  FileMetaData getCurrentFileMetaData(){return currentFileMetaData;}

  Size_t maxDataInPacket(NAVDAT_MultiplexCfg_t cfgMux);
  void saveToFile(NAVDAT_MultiplexCfg_t cfgMux, QString path, QString fileName, QByteArray fileData, quint32 mmsi = 0, qint16 fileId = -1, quint16 timestamp = 0);
  void transmit(NAVDAT_MultiplexCfg_t cfgMux, QString fileName, QByteArray fileData, quint32 mmsi = 0, qint16 fileId = -1, quint16 timestamp = 0);

public slots:
  void reset();

signals:
  void frame_transmitted(int fileId, int id, bool isFirst, bool isLast);

private:
  struct FrameData
  {
    int idFile, id;
    bool isFirst, isLast;
    NAVDAT_RawFrame_t* buf;
  };

  QString SenderID;
  UInt8_t counterFileId;

  NAVDAT_Multiplex_t Mux;
  NAVDAT_Codec_t Cod;
  QByteArray FileNameBuf;
  NAVDAT_Frame_t converterFrame;

  NAVDAT_Modulator_t Mod;
  FileMetaData currentFileMetaData;
  FrameData currentFrameData;

  QList<FileMetaData> filesMetaData;
  QList<FrameData> framesData;

  QVector<UInt8_t> Data;
  NAVDAT_qamData_t rawData;
  QVector<qamSymbol_t> misDataBuf, tisDataBuf, dsDataBuf;

  void init_converter(NAVDAT_MultiplexCfg_t cfgMux);
  void convert_file(quint32 mmsi, QString fileName, QByteArray fileData, qint16 fileId = -1, quint16 timestamp = 0);
  void deinit_converter();

  Size_t prepare_frame(quint32 mmsi, quint8 counterFileId, UInt16_t id, quint16 timestamp, bool isFirst, Size_t restDataSize);
  void convert_packet(UInt8_t* data, Size_t size, NAVDAT_RawFrame_t* converterFrame, NAVDAT_RawFrame_t* encFrame);

  bool write_next_frame_to_modulator();
  void write_frame_to_modulator(NAVDAT_RawFrame_t *converterFrame);

  void stream_out(iqSample_t* samples, Size_t count);
};

Q_DECLARE_METATYPE(NAVDAT_TIS_qamMode_t)
Q_DECLARE_METATYPE(NAVDAT_DS_qamMode_t)
Q_DECLARE_METATYPE(NAVDAT_DS_Code_t)


#endif // QSDRNAVDATTRANSMITTER_H
