#ifndef QSDRTESTTRANSMITTER_H
#define QSDRTESTTRANSMITTER_H

#include "QSdrTransmitter.h"
#include <QVector>

#include "SDR/BASE/Sources/Generator.h"

class QSdrTestTransmitter : public QSdrTransmitter
{
  Q_OBJECT
public:
  enum Mode{mode_Constant, mode_Generator};

  explicit QSdrTestTransmitter(Frequency_t Fd, int GeneratorBufSize, QObject* parent = 0);

  void setMode(Mode mode){this->mode = mode;}
  void reset();

private:
  Mode mode;

  Generator_t Generator;
  QVector<Sample_t> GeneratorBuf;

  void stream_out(iqSample_t* samples, Size_t count);
};

#endif // QSDRTESTTRANSMITTER_H
